var express = require('express');
var logger = require('morgan');
var bodyParser = require('body-parser');

/*
 * This module acts as entry for the server
 */

var app = express();
var port = process.env.PORT || 3000;

// Configuration
app.use(express.static(__dirname + '/public'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Routes
require('./routes/routes.js')(app);
app.listen(port); 
console.log('VirtualFence server is running on port: ' +port);